local script = "https://raw.githubusercontent.com/Panakotta00/FicsIt-OS/main/misc/install.lua"

print("Load internet...")
internet = computer.getPCIDevices(findClass("FINInternetCard"))[1]
if not internet then
    print("ERROR! No internet-card found! Please install a internet card!")
    computer.beep(0.2)
    return
end

print("Download Installation script...")
code, data = internet:request(script, "GET", ""):await()
if code ~= 200 then
    print("ERROR! Failed to request installation script from '" .. script .. "'")
    computer.beep(0.2)
    return
end

print("Run installation...")
func = load(data)
func()
