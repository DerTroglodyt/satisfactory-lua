-- Fetches all libs / boot files from GIT and sends them over network on request.
--
-- Network should look something like this:
--           Computer
--              |
--            Router
--              |
--              |
-- LibServer ---+--- Router - Computer
--              |
--              |
--            Router
--              |
--           Computer
--
-- Routers aren't required but help keep component list in network short.
-- See eeprom.lua for how to fetch libs / boot files. (Is done automaticly on boot)

local libs = {}
local gitRoot = "https://gitlab.com/DerTroglodyt/satisfactory-lua/-/raw/main/src/"
-- get internet card
local inet = computer.getPCIDevices(findClass("FINInternetCard"))[1]
local nc = computer.getPCIDevices(findClass("NetworkCard"))[1]

-- get library from internet and write it to /lib/<name>. returns true if succesful or error string.
function fetchLib(libName)
    local name = libName..".lua"
    local req = inet:request(gitRoot..name, "GET", "")
    local result, libdata = req:await()
    if (result ~= 200) then
        print(result)
        print(libdata)
        return "Fetch of library <"..path.."> failed!"
    end
    libs[libName] = libdata
end

function fetchAllLibs()
    local libNames = {"json", "boot", "ed", "fp", "test", "util", "tui", "bootAlHuette"}
    for _, name in ipairs(libNames) do
        print("Fetching "..name.."...")
        fetchLib(name)
    end
end


-- Provide libraries over network to other cpus
fetchAllLibs()
nc:open(80)
event.clear()
event.listen(nc)
print("starting lib server...")
while true do
    local e, s, p1, p2, p3 = event.pull()
    --    print("Event:"..tostring(e).." s:"..tostring(s).." p1:"..tostring(p1).." p2:"..tostring(p2))
    if e == "NetworkMessage" then
        local rcv = p1
        local port = p2
        local x = p3:find("#")
        print("received msg from " .. rcv .. ":" .. port .. " -> " .. p3)
        local cmd = p3:sub(1,x-1)
        local msg = p3:sub(x+1)
        if cmd == "GETLIB" then
            local libData = libs[msg]
            if libData ~= nil then
                nc:send(rcv, port, "SENDLIB#"..libData)
            else
                nc:send(rcv, port, "ERROR#Unknown library '"..msg.."'!")
            end
        elseif cmd == "REFETCH" then
            print("Refetching all libs...")
            fetchAllLibs()
            nc:send(rcv, port, "REFETCHDONE#Ok.")
        else
            nc:send(rcv, port, "ERROR#Unknown command '"..cmd.."'!")
        end
    end
end
