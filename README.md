# LUA scripts for the Satisfactory mod Ficsit-Networks

###To use this repository:

You need a computer with one drive and a GPU.
Copy and paste the content of 'eeprom.lua' into your EEPROM and 
switch your computer on.
This will download the boot.lua from this repository and run it.
File system:
/dev All devices
/lib All downloaded libs
ToDo:
You should get a TextUI to manage / edit your lua files.  

### Git root

```
https://gitlab.com/DerTroglodyt/satisfactory-lua.git
```

## License
Public Domain. Do what you want with it.
