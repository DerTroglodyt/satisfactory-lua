-- Eventually will be a library for a textual user interface.

local util = bios.loadLib("util")

local wx, wy = 0, 0
local ww, wh = gpu:getSize()
local cx, cy = 0, 0
local windows = util.newStack()
local activeWin
local colors = {
    frame = { 0, 0.5, 0.8, 1 },
    text = { 1, 1, 1, 1 },
    warn = { 1, 1, 0, 1 },
    error = { 1, 0, 0, 1 }
}

function drawFrame(write, x, y, w, h, flush)
    for ix=1, w-2 do
        write(x+ix, y, "-", false)
        write(x+ix, y+h-1, "-", false)
    end
    for iy=1, h-2 do
        write(x, y+iy, "|", false)
        write(x+w-1, y+iy, "|", false)
    end
    write(x, y, "+", false)
    write(x+w-1, y, "+", false)
    write(x+w-1, y+h-1, "+", false)
    write(x, y+h-1, "+", flush)
end

local Window = function (title, posX, posY, width , height, framed)
    local self = {
        title = title,
        posX = posX or 0,
        posY = posY or 0,
        width = width or 120,
        height = height or 40,
        content = {},
        cx = 0,
        cy = 0
    }
    for x=1, self.width do
        self.content[x] = {}
        for y=1, self.height do
            self.content[x][y] = " "
        end
    end

    function moveCursor(dir)
        if dir == "right" then
            cx = cx + 1
            if cx > width then
                cx = 0
                cy = cy + 1
            end
        end
    end

    local write = function (x, y, text, flush)
        local t = tostring(text):sub(1, self.width)
        for i=1, #t do
            self.content[i][y] = t[i]
        end
        gpu:setText(self.posX + x, self.posY + y, t)
        if flush then gpu:flush() end
    end

    local restore = function (flush)
        for x=1, width do
            for y=1, height do
                gpu:setText(self.posX + x, self.posY + y, self.content[x][y])
            end
        end
        if flush then gpu:flush() end
    end

    local onKeyChar = function (c)
        write(cx, cy, c, true)
        moveCursor("right")
    end

    gpu:setForeground(util.unpack(colors.frame))
    if framed then
        drawFrame(write, 0, 0, self.width, self.height, false)
    end
    if title ~= nil then write(2, 0, title, false) end
    gpu:setForeground(util.unpack(colors.text))

    return {
        write = write,
        restore = restore,
        onKeyChar = onKeyChar
    }
end

return {
    Window = Window
}
