--require("mock")
local tui = bios.loadLib("tui")

print("PROGRAM bootAlHuette started.")

local win = tui.Window("Aluminium Hütte Nordrand")
win.write(0, 2, "Energy cons.")
win.write(0, 3, "Production:")
win.write(2, 4, "Alumina")
win.write(2, 5, "Al Scrap")
win.write(2, 6, "Al Ingot")
win.write(2, 7, "Al Case")
win.write(2, 8, "Al Allclad")

win.write(30, 3, "Resources:")
win.write(32, 4, "Water")

function format(act, max)
    return string.format("%.0f", act/max*100) .. "% (".. act .. ")"
end

function update(energy, alumina, scrap, ingot, case, allclad, water)
    win.write(13, 2, energy)

    win.write(13, 4, format(alumina, 240))
    win.write(13, 5, format(scrap, 240))
    win.write(13, 6, format(ingot, 240))
    win.write(13, 7, format(case, 120))
    win.write(13, 8, format(allclad, 240))

    win.write(42, 4, format(water, 3200))
end

while true do
    update(1200, 235, 200, 180, 50, 100, 3000)
    gpu:flush()
    util.sleep(1000)
end

print("PROGRAM bootAlHuette ended.")
