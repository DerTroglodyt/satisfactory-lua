util = {
    newStack = function()
        local self = { stack = {} }

        local push = function(value)
            table.insert(self.stack, item)
        end

        local pop = function()
            return table.remove(self.stack)
        end

        return {
            push = push,
            pop = pop
        }
    end,

    sleep = function(milliSeconds)
        event.pull(milliSeconds)
    end,

    unpack = function(t, i)
        i = i or 1
        if t[i] ~= nil then
            return t[i], util.unpack(t, i + 1)
        end
    end
}

return util
