--require("eeprom")
local util = bios.loadLib("util")

local wx, wy = 0, 0
local ww, wh = gpu:getSize()
local cx, cy = 0, 0
local windows = util.newStack()
local activeWin

function drawFrame(write, x, y, w, h, flush)
    for ix=1, w-2 do
        write(x+ix, y, "-", false)
        write(x+ix, y+h-1, "-", false)
    end
    for iy=1, h-2 do
        write(x, y+iy, "|", false)
        write(x+w-1, y+iy, "|", false)
    end
    write(x, y, "+", false)
    write(x+w-1, y, "+", false)
    write(x+w-1, y+h-1, "+", false)
    write(x, y+h-1, "+", flush)
end

function newWindow(posX, posY, width , height, framed)
    local self = {
        posX = posX, posY = posY, width = width, height = height, content = {},
        cx = 0, cy = 0
    }
    for x=1, width do
        self.content[x] = {}
        for y=1, height do
            self.content[x][y] = " "
        end
    end

    function moveCursor(dir)
        if dir == "right" then
            cx = cx + 1
            if cx > width then
                cx = 0
                cy = cy + 1
            end
        end
    end

    local write = function (x, y, text, flush)
        local t = text:sub(1, self.width)
        for i=1, #text do
            self.content[i][y] = text[i]
        end
        gpu:setText(self.posX + x, self.posY + y, t)
        if flush then gpu:flush() end
    end

    local restore = function (flush)
        for x=1, width do
            for y=1, height do
                gpu:setText(self.posX + x, self.posY + y, self.content[x][y])
            end
        end
        if flush then gpu:flush() end
    end

    local onKeyChar = function (c)
        write(cx, cy, c, true)
        moveCursor("right")
    end

    if framed then
        drawFrame(write, 0, 0, self.width, self.height, false)
    end

    return {
        write = write,
        restore = restore,
        onKeyChar = onKeyChar
    }
end

function newDialog(text)
    self = {}
    if activeWin ~= nil then
        windows.push(activeWin)
    end
    local bh = (wh / 2) - 3
    local bw = (ww / 2) - ((#text+4) / 2)
    activeWin = newWindow(bw, bh, #text+4, 5, true)
    activeWin.write(2, 2, text)

    local close = function ()
        activeWin = windows.pop()
        activeWin.restore()
    end

    local onKeyChar = function (_)
        self.close()
    end

    return {
        close = close,
        onKeyChar = onKeyChar
    }
end

activeWin = newWindow(0, 0, ww, wh, false)
windows.push(mainWin)
for x=0, ww-1 do
    activeWin.write(x, 0, "=", false)
end

for x=0, ww-1 do
    for y=1, wh-1 do
        activeWin.write(x, y, ".", false)
    end
end
activeWin.write(3, 0, " ED: <filename.lua> ", true)

local dlg = newDialog("Welcome to ED!")

while true do
    local e, s, p1, p2 = event.pull()
    print("Event:"..tostring(e).." s:"..tostring(s).." p1:"..tostring(p1).." p2:"..tostring(p2))
    if e == "OnKeyChar" then
        activeWin.onKeyChar(p1)
    end
end
