function calcPercent(value, min , max)
    return (value - min) / (max - min)
end

function pot2percent(potentiometer)
    return calcPercent(potentiometer.value, potentiometer.min, potentiometer.max)
end

-- fetch latest version from gitlab repository
--bios.refetchLib("json.lua")
-- load the library from the file system and use it
--local json = fs.doFile("/libs/json.lua")
--
-- local data = {
--   somekey = "is stuff",
--   otherkey = 42
-- }
--
-- print(json.encode(data))

--local BigCompPanel = component.proxy("E8077B7244C361AC00BCDFB7AE243203")
local modules = BigCompPanel:getModules()
--for i, m in pairs(modules) do
--  print(m)
--end

local lamp1 = BigCompPanel:getModule(0, 1, 1)
lamp1:setColor(0.3, 0.3, 0.8, 0.0)
local switch1 = BigCompPanel:getModule(0, 0, 1)
event.listen(switch1)

local lamp2 = BigCompPanel:getModule(1, 1, 1)
lamp2:setColor(0.3, 0.3, 0.0, 0.0)
local switch2 = BigCompPanel:getModule(1, 0, 1)
event.listen(switch2)

local signalLmp = SignalLamp:getModule(0,0)
signalLmp:setColor(0.0, 0.0, 1.0, 0.0)
local signalBtn = SignalButton:getModule(0,0)
signalBtn:setColor(1.0, 1.0, 0.0, 0.5)
event.listen(signalBtn)

local potiGauge = BigCompPanel:getModule(3, 2, 1)
local potiLabel = BigCompPanel:getModule(3, 1, 1)
local potentiometer = BigCompPanel:getModule(3, 0, 1)
potentiometer.min = 0
potentiometer.max = 10
potentiometer.value = 5
potiGauge.limit = calcPercent(8, 0, 10)
potiGauge.percent = pot2percent(potentiometer)
potiLabel:setText(potentiometer.value)
event.listen(potentiometer)

gpu:setText(0, 0, "Press 'ö' to exit test.lua.")
gpu:flush()

while true do
    local e, s, p1, p2 = event.pull()
    print("Event:"..tostring(e).." s:"..tostring(s).." p1:"..tostring(p1).." p2:"..tostring(p2))
    if e == "ChangeState" then
        if s == switch1 then
            lamp1:setColor(0.3, 0.3, 0.8, p1 and 0.5 or 0.0)
        end
        if s == switch2 then
            lamp2:setColor(0.3, 0.3, 0.0, p1 and 0.5 or 0.0)
        end
        if s == signalBtn then
            signalLmp:setColor(1.0, 0.0, 0.0, p1 and 0.5 or 0.0)
        end
    end
    if e == "valueChanged" then
        if s == potentiometer then
            potiLabel:setText(p1)
            potiGauge.percent = pot2percent(potentiometer)
        end
    end
    if e == "OnKeyChar" and p1 == "ö" then
        return
    end
end
