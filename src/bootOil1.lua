-- Starts / stops second plastic refinery depending on rubber container fill.
print("Refinery controller started.")
local lastState
while true do
    local inv = RubberCont:getInventories()[1]
    --  print("RubberCont full: "..(inv.itemCount/200).."  "..inv.size)
    local isFull = (inv.itemCount/200) == inv.size
    if isFull ~= lastState then
        lastState = isFull
        if isFull then
            print("Starting refinery")
            Refinery1.standby = false
        else
            print("Stopping refinery")
            Refinery1.standby = true
        end
    end
    event.pull(5)
end
print("Refinery controller ended.")
