local unpack = table.unpack or unpack

local loop = function (predicate, static, next, shouldBreak)
    local key, value, state

    repeat
        key, value = next(static, key)

        if not (key == nil) then
            state = predicate(value, key)
        end
    until (key == nil) or (shouldBreak and shouldBreak(state))

    return state
end

local iterate = function (predicate, iteratee, initial, shouldBreak)
    return iteratee and (type(iteratee) == 'table')
            and loop(predicate, iteratee, pairs(iteratee), shouldBreak)
            or initial
end

local function createNextArgsPredicate(prefixedArgs)
    local args = {unpack(prefixedArgs)}

    return function (arg)
        args[#args + 1] = arg
        return args
    end
end

local function createPrefixed(func, prefixedArgs)
    return function(...)
        local nextArgsPredicate = createNextArgsPredicate(prefixedArgs)
        local args = iterate(nextArgsPredicate, {...})

        return func(unpack(args))
    end
end

local curry = function (func, arity)
    return function(...)
        if arity <= 1 then
            return func(...)
        end

        local args = {...}

        if #args < arity then
            local prefixed = createPrefixed(func, args)

            return curry(prefixed, arity - #args)
        end

        return func(...)
    end
end

local function createClonePredicate()
    local cloned = {}

    return function (value, key)
        cloned[key] = value
        return cloned
    end
end

local function clone (base)
    local clonePredicate = createClonePredicate()
    return iterate(clonePredicate, base, {})
end

local function createConcatPredicate(base)
    local concatenated = {unpack(base)}

    return function (value)
        concatenated[#concatenated + 1] = value
        return concatenated
    end
end

local function concat(base, iteratee)
    local concatPredicate = createConcatPredicate(base)
    return iterate(concatPredicate, iteratee)
end

local function createEachPredicate(predicate)
    return function (value)
        return predicate(value)
    end
end

local function each(predicate, iteratee)
    local eachPredicate = createEachPredicate(predicate)

    return iterate(eachPredicate, iteratee)
end

return curry(each, 2)

local function isBoolean(value)
    return type(value) == 'boolean'
end

local function isFalse(value)
    return value == false
end

local function isFalsy(value)
    return not value
end

local function isFunction(value)
    return type(value) == 'function'
end

local function is(first, second)
    return first == second
end

local function isNil(value)
    return value == nil
end

local function isNumber(value)
    return type(value) == 'number'
end

local function isString(value)
    return type(value) == 'string'
end

local function isTable(value)
    return type(value) == 'table'
end

local function isTrue(value)
    return value == true
end

local function isTruthy(value)
    return not isFalsy(value)
end

local function mergeDeep(createPredicate)
    return function (base, values)
        local mergeDeepPredicate = createPredicate(base)
        return iterate(mergeDeepPredicate, values)
    end
end

local function createMergeDeepPredicate(base)
    local merged = clone(base)

    return function (value, key)
        if isTable(value) and merged[key] and isTable(merged[key]) then
            merged[key] = mergeDeep(createMergeDeepPredicate)(merged[key], value)
        else
            merged[key] = value
        end

        return merged
    end
end

local function createMergePredicate(base)
    local merged = clone(base)

    return function (value, key)
        merged[key] = value
        return merged
    end
end

local function merge(base, values)
    local mergePredicate = createMergePredicate(base)
    return iterate(mergePredicate, values)
end



local function createFindPredicate(predicate)
    local found = nil

    return function (value)
        if predicate(value) then
            found = value
        end
        return found
    end
end

local function find(predicate, iteratee)
    local findPredicate = createFindPredicate(predicate)

    return iterate(findPredicate, iteratee, nil, isTruthy)
end

local function createGetPredicate(base)
    local pointer = base

    return function (key)
        pointer = pointer[key]

        return pointer
    end
end

local function getUsingKeyTable(keyTable, base)
    local predicate = createGetPredicate(base)

    return iterate(predicate, keyTable, nil, isNil)
end

local function get(key, base)
    if isTable(key) then
        return getUsingKeyTable(key, base)
    end
    return base and base[key] or nil
end

local function createMapPredicate(predicate)
    local mapped = {}

    return function (value, key)
        mapped[key] = predicate(value)
        return mapped
    end
end

local function map(predicate, iteratee)
    local mapPredicate = createMapPredicate(predicate)
    return iterate(mapPredicate, iteratee)
end

local function negate(predicate)
    return function(value)
        return not predicate(value)
    end
end

local function createPipePredicate(initial)
    local state = initial

    return function (func)
        state = func(state)
        return state
    end
end

local pipe = function (...)
    local funcs = {...}

    return function (initial)
        local pipePredicate = createPipePredicate(initial)
        return iterate(pipePredicate, funcs, initial)
    end
end

local function createReducePredicate(predicate, initial)
    local state = initial

    return function (value)
        state = predicate(state, value)
        return state
    end
end

local function reduce(predicate, initial, iteratee)
    local reducePredicate = createReducePredicate(predicate, initial)
    return iterate(reducePredicate, iteratee, initial)
end

local function createReversePredicate(predicate)
    local reversed = {}

    return function (value)
        table.insert(reversed, 1, value)
        return reversed
    end
end

local function reverse(iteratee)
    local reversePredicate = createReversePredicate()
    return iterate(reversePredicate, iteratee)
end

local function setUsingTableKey(keyTable, value, base, index)
    local subTable = base or {}
    local key = keyTable[index]

    if index == #keyTable then
        return merge(subTable, {[key] = value})
    end

    return merge(subTable, {
        [key] = setUsingTableKey(keyTable, value, subTable[key], index + 1)
    })
end

local function set(key, value, base)
    if isTable(key) then
        return setUsingTableKey(key, value, base, 1)
    end

    if base and base[key] == value then
        return base
    end

    return merge(base, {[key] = value})
end

local function createSplitPredicate(separator, pattern)
    local splitted = {}

    return function (__, substr)
        splitted[#splitted + 1] = string.gsub(substr, pattern, '')
        return splitted
    end
end

local function split(separator, str)
    local pattern = '%' .. separator
    local next = string.gmatch(separator .. str, pattern .. '[^' .. pattern .. ']*')
    local splitPredicate = createSplitPredicate(separator, pattern)

    --return loop(splitPredicate, nil, next, nil, {})
    return loop(splitPredicate, nil, next, {})
end



return {
    boolean = curry(isBoolean, 1),
    curry = curry,
    clone = curry(clone, 1),
    concat = curry(concat, 2),
    each = curry(each, 2),
    find = curry(find, 2),
    get = curry(get, 2),
    is = curry(is, 2),
    isFalse = curry(isFalse, 1),
    isFalsy = curry(isFalsy, 1),
    isFunction = curry(isFunction, 1),
    isNil = curry(isNil, 1),
    isNumber = curry(isNumber, 1),
    isString = curry(isString, 1),
    isTable = curry(isTable, 1),
    isTrue = curry(isTrue, 1),
    isTruthy = curry(isTruthy, 1),
    iterate = iterate,
    loop = loop,
    map = curry(map, 2),
    merge = curry(merge, 2),
    mergeDeep = curry(mergeDeep(createMergeDeepPredicate), 2),
    negate = curry(negate, 1),
    pipe = pipe,
    reduce = curry(reduce, 3),
    reverse = curry(reverse, 1),
    set = curry(set, 3),
    split = curry(split, 2)
}
