-- Starts computer and loads libs from LibServer.
--
-- Copy this into your eeproms. Computer needs a gpu and network card (not internet card).
-- If drives are found the first one gets mounted as "/".
-- If an external screen is found it is bound to the gpu.
--
-- Don't edit this file. Use a boot file instead.
-- That keeps your local functionality in the boot file without the bios bloat.
-- Change name of boot file you want to run. Omit ".lua" ending.
local bootFile = "bootOil1"

--require("mock")

fs = filesystem
-- ID of Master Control Program (not required)
mcpNC = "011294524387C84F4564F6B49B56C05C"
-- ID of your LibServer. REQUIRED!
local libSvrNC = "4CD9552645E8B481944256845F316033"
local netCard = computer.getPCIDevices(findClass("NetworkCard"))[1]
netCard:open(80)
event.clear()
event.listen(netCard)




local getLib = function (libName)
  while true do
    netCard:send(libSvrNC, 80, "GETLIB#"..libName)
    local e, s, p1, p2, p3 = event.pull(5)
    --    print("Event:"..tostring(e).." s:"..tostring(s).." p1:"..tostring(p1).." p2:"..tostring(p2))
    if e == "NetworkMessage" then
      local rcv = p1
      local port = p2
      --print("received msg from " .. rcv .. ":" .. port .. " -> " .. p3)
      if p3:sub(1,7) == "SENDLIB" then
        return p3:sub(9)
      else
        print(p3)
        return nil
      end
    end
  end
end

bios = {
  _version = "0.1",

  -- get library from LibServer and execute it.
  loadLib = function (name)
    local libData = getLib(name)
    if libData == nil then
      return nil
    end
    local f = fs.open("/tmp/libdata.lua", "w")
    f:write(libData)
    f:close()
    return fs.doFile("/tmp/libdata.lua")
  end,

  -- Adds all components in the network to the script as variables with the first nick as name.
  comp2var = function ()
    local comps = component.proxy(component.findComponent(""))
    for _,comp in pairs(comps) do
      local name = string.match(comp.nick, "^[a-zA-Z][a-zA-Z0-9_]*")
      if name then
        _G[name] = comp
      end
    end
  end,

  -- clear background
  clearScr = function ()
    gpu:setBackground(0,0,0,1)
    gpu:setForeground(1,1,1,1)
    local w, h = gpu:getSize()
    gpu:fill(0, 0, w, h, " ")
    gpu:flush()
  end
}






print("init graphics...")
gpu = computer.getPCIDevices(findClass("GPUT1"))[1]
if not gpu then
  error("No GPU T1 found!")
end
-- get first Screen-Driver available from PCI-Interface
local screen = computer.getPCIDevices(findClass("FINComputerScreen"))[1]
-- if no screen found, try to find large screen from component network
if not screen then
  local comp = component.findComponent(findClass("Screen"))[1]
  if not comp then
    error("No screen found!")
  end
  screen = component.proxy(comp)
end

-- setup gpu
event.listen(gpu)
gpu:bindScreen(screen)
gpu:setSize(100, 40)
bios.clearScr()

print("init file system...")
fs.initFileSystem("/dev")

print("create temporary file system...")
fs.makeFileSystem("tmpfs", "tmp")
fs.mount("/dev/tmp","/tmp")

-- find first drive
drives = fs.childs("/dev")
-- Filtering out "serial"
for idx, drive in pairs(drives) do
  if drive == "serial" then table.remove(drives, idx) end
end

if #drives > 0 then
  print("mount first drive '"..drives[1].."' as root...")
  fs.mount("/dev/"..drives[1], "/")
else
  print("No drives found. Only temporary file system available.")
end

print("create component variable names...")
bios.comp2var()

computer.beep(5)

-- Use next 3 lines to load changes from git when debugging away from LibServer and editing out of game.
--netCard:send(libSvrNC, 80, "REFETCH#")
--local e, s, p1, p2, p3 = event.pull(5)
--print("received msg from " .. p1 .. ":" .. p2 .. " -> " .. p3)

print("run "..bootFile..".lua...")
bios.loadLib(bootFile)

bios.clearScr()
print()
print("EEPROM done.")
