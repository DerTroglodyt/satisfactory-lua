-- Mockup of the lua core in satisfactory networks.
-- For testing outside of the game.
-- Install a lua executable and use your favourite IDE.
-- Use 'require("mock")' in the boot file you are debugging.

require("socket")

bios = {
    loadLib = function(name)
        return require(name)
    end
}

filesystem = {
    rootPath = "",
    initFileSystem = function (path) filesystem.rootPath = path end,
    mount = function (a, b) end,
    makeFileSystem = function (a, b) end,
    childs = function (path) return { "devID" } end,
    exists = function (path) return false end,
    createDir = function (path) return true end,
    open = function (path, option) return nil end,
    doFile = function (path)
        local p = path:sub(6)
        if p:find(".") == nil then p = p .. ".lua" end
        return dofile(p)
    end
}

event = {
    eventKeys = "Hallo da draußen!",
    eventKeyPos = 0,
    clear = function () end,
    listen = function (gpu) end,
    --pull = function ()
    --    event.eventKeyPos = event.eventKeyPos + 1
    --    if event.eventKeyPos <= #event.eventKeys then
    --        return "OnKeyChar", "Blah", event.eventKeys:sub(event.eventKeyPos, event.eventKeyPos)
    --    else
    --        error("End of eventKeys reached.")
    --    end
    pull = function (maxWaitSeconds)
        if maxWaitSeconds ~= nil then
            socket.select(nil, nil, maxWaitSeconds/1000)
        else
            event.eventKeyPos = event.eventKeyPos + 1
            if event.eventKeyPos <= #event.eventKeys then
                return "NetworkMessage", "Blah", "2587345973458374597835", 80, "print(this could be your data)"
            else
                error("End of eventKeys reached.")
            end
        end
    end
}

function findClass(c)
    return tostring(c)
end

component = {
    screen = {},
    findComponent = function (c)
        if c == "" then
            return {component.screen}
        elseif c == "Screen" then
            return {component.screen}
        end
        return error("No component '"..tostring(c).."' known!")
    end,
    proxy = function(list) return {} end
}

gpu = {
    content = {}
}

function gpu:bindScreen(screen) end

function gpu:setText(x, y, s)
    for tx=1, #s do
        gpu.content[y+1][x+tx] = s:sub(tx, tx)
    end
end

function gpu:fill(x, y, dx, dy, c)
    local ch = c:sub(1, 1)
    for ty=y, y+dy do
        for tx=y, x+dx do
            gpu.content[ty+1][tx+1] = ch
        end
    end
end

function gpu:flush()
    for y=1, 40 do
        local line = ""
        for x=1, 120 do
            line = line .. gpu.content[y][x]
        end
        print(line)
    end
end

function gpu:getSize()
    return 120, 40
end
function gpu:setSize(w, h) end
function gpu:setBackground(r, g, b, t) end
function gpu:setForeground(r, g, b, t) end

netCard = {
    open = function (port) end,
    send = function (receiver, port, msg) end
}

computer = {
    inetCard = {},
    netCard = netCard,
    gpu = gpu,
    screen = {},
    beep = function (x) end,
    getPCIDevices = function (clazz)
        if clazz == "FINInternetCard" then
            return {computer.inetCard}
        elseif clazz == "GPUT1" then
            return {computer.gpu}
        elseif clazz == "FINComputerScreen" then
            return {computer.screen}
        elseif clazz == "NetworkCard" then
            return {computer.netCard}
        end
        return error("No device class '"..tostring(clazz).."' known!")
    end
}
computer.inetCard.request = function (t, m, s)
    return {
        await = function () return 200, "200" end
    }
end


for y=1, 50 do
    gpu.content[y] = {}
    for x=1, 150 do
        gpu.content[y][x] = "."
    end
end

-- Test
--gpu.setText(2, 2, "Hallo")
--gpu.flush()
